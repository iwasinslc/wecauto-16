<?php
return [
    'registration_without_ref_disabled' => env('TELEGRAM_REGISTRATION_WITHOUT_REF_DISABLED', false),
];