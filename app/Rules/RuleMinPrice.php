<?php
namespace App\Rules;

use App\Models\Rate;
use App\Models\Setting;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class RuleCheckRate
 * @package App\Rules
 */
class RuleMinPrice implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $min = 0.00000001;

        $mainWallet = user()->wallets()->find(request()->main_wallet_id);


        if ($mainWallet->currency->code!='FST')
        {
            $min = (float) Setting::getValue('min_price_usd');
            if ($min==0) return true;
        }


        return $value>=$min;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Вы указали меньше допустимой цены';
    }
}
