<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Withdrawal",
 *     description="Withdrawals information"
 * )
 */
class Withdrawal
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="Withdrawal ID",
     *     example="b5d00f20-ff1d-11ea-8e8c-211d59c5f58c"
     * )
     *
     * @var string
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Payment System",
     *     description="Paymrny system name",
     *     example="wecapicoin"
     * )
     *
     * @var string
     */
    public $payment_system;

    /**
     * @OA\Property(
     *     title="Value",
     *     description="Withdrawal amount",
     *     example=1000.00
     * )
     *
     * @var float
     */
    public $value;

    /**
     * @OA\Property(
     *     title="Currency",
     *     description="Currency code of the withdrawal amount",
     *     example="WEC"
     * )
     *
     * @var string
     */
    public $currency;

    /**
     * @OA\Property(
     *     title="Status",
     *     description="Status of the withdrawal",
     *     example="Error"
     * )
     *
     * @var string
     */
    public $status;


    /**
     * @OA\Property(
     *     title="Batch id",
     *     description="Batch id of the withdrawal",
     * )
     *
     * @var string
     */
    public $batch_id;

    /**
     * @OA\Property(
     *     title="Source",
     *     description="Source of the withdrawal",
     * )
     *
     * @var string
     */
    public $source;

    /**
     * @OA\Property(
     *     property="result",
     *     title="Result",
     *     description="Result information for transactions",
     *     type="string"
     * )
     * @var string
     */
    public $result;

    /**
     * @OA\Property(
     *     title="Created At",
     *     description="Date of the creating withdrawal",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $created_at;
}