<?php
namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
//use PragmaRX\Google2FALaravel\Support\Authenticator;

class DepositLicence
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $active = \user()->activeLicence();

        if (!$active)
        {
            return redirect()->route('profile.profile')->with('error', __('You do not have an active license.'));

        }


        if (user()->checkDepositLimit())
        {
            return redirect()->route('profile.profile')->with('error', __('The deposit limit has been exhausted. Buy a new license.'));
        }

        return $next($request);



    }
}
