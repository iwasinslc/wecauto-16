<?php
namespace App\Http\Controllers\Admin;


use App\Models\Licences;
use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class ProjectsController
 * @package App\Http\Controllers\Admin
 */
class ProjectsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $projects = Project::orderBy('created_at')->get();

        return view('admin.projects.index', [
            'projects' => $projects,
        ]);
    }

    /**
     * @param string $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $id)
    {
        /** @var Project $project */
        $project = Project::find($id);

        if (null == $project) {
            abort(404);
        }

        return view('admin.projects.edit', [
            'project' => $project,
        ]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * @param Request $request
     * @return $this|RedirectResponse
     */
    public function store(Request $request)
    {
        $project = new Project();

        if (!$project) {
            return back()->with('error', __('Unable to create tariff plan'))->withInput();
        }

        $project->name                  = strip_tags($request->name);
        $project->url      = strip_tags($request->url);
        $project->description     = strip_tags($request->description);
        $project->save();

        if ($request->hasFile('img')) {
            $project->addImg($request->file('img'));
        }

        return redirect()->route('admin.projects.index')->with('success', __('Licence plan has been created'));
    }


    /**
     * @param \Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, string $id)
    {
        /** @var Project $project */
        $project = Project::find($id);

        if (null == $project) {
            abort(404);
        }



        $project->name                  = strip_tags($request->name);
        $project->url      = strip_tags($request->url);
        $project->description     = strip_tags($request->description);



        $project->save();

        if ($request->hasFile('img')) {
            $project->addImg($request->file('img'));
        }

        return back()->with('success', 'Проект успешно обновлена')->withInput();
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(string $id)
    {
        /** @var Licences $project */
        $project = Project::find($id);

        if (null == $project) {
            abort(404);
        }

        if ($project->delete()) {
            return back()->with('success', 'Проект успешно удалена');
        }
        return back()->with('error', 'Проект не может быть удален');
    }
}
