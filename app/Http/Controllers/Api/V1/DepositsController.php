<?php


namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RequestDataTable;
use App\Http\Resources\DepositResource;
use App\Models\Deposit;

class DepositsController extends Controller
{
    /**
     * @OA\Get(
     *      path="/deposits",
     *      operationId="getDeposits",
     *      tags={"Deposits"},
     *      summary="Get list of the deposits",
     *      description="Return array of the deposits. Filters: user_id",
     *      @OA\Parameter(
     *          ref="#/components/parameters/user_id"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/DepositResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(RequestDataTable $request) {
        return DepositResource::collection(
                Deposit::with(['currency'])
                    ->where('user_id', $request->user_id)->get()
            );
    }

}