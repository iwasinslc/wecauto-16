<?php
namespace App\Modules;

use GuzzleHttp\Client;

/**
 * Class LivecoinModule
 * @package App\Modules
 */
class LivecoinModule
{
    /** @var string $api */
    private $api = 'https://api.livecoin.net/';

    /**
     * @param string $method
     * @param string|null $type
     * @param array|null $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
     */
    public function sendRequest(string $method, string $type=null, array $data=null)
    {
        if (null === $type) {
            $type = 'GET';
        }

        if (null === $data) {
            $data = [];
        }

        $client   = new Client();
        $baseUrl  = $this->api;
        $headers  = [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];
        $verify   = config('app.env') == 'production' ? true : false;
        $params   = [
            'headers' => $headers,
            'verify'  => $verify,
        ];

        if (!empty($data)) {
            $params['form_params'] = $data;
        }

        try {
            $response = $client->request($type, $baseUrl.$method, $params);
        } catch (\Exception $e) {
            throw new \Exception('LivecoinModule API request is failed. '.$e->getMessage());
        }

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('LivecoinModule API response status is '.$response->getStatusCode().' for method '.$method);
        }

        $body = json_decode($response->getBody()->getContents());

        return $body;
    }

    /**
     * @param string $pair
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getRate(string $pair)
    {
        try {
            $response = (new LivecoinModule())->sendRequest('exchange/ticker?currencyPair='.$pair, 'GET');
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $response;
    }
}