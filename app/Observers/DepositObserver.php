<?php
namespace App\Observers;

use App\Models\Deposit;
use App\Models\DepositQueue;

/**
 * Class DepositObserver
 * @package App\Observers
 */
class DepositObserver
{
    /**
     * @param Deposit $deposit
     */
    public function deleting(Deposit $deposit)
    {
        foreach ($deposit->transactions()->get() as $transaction) {
            $transaction->delete();
        }

        foreach (DepositQueue::where('deposit_id', $deposit->id)->get() as $queue) {
            $queue->delete();
        }
    }

    /**
     * @param Deposit $deposit
     * @param bool $depositClosed
     * @return array
     * @throws \Exception
     */
    private function getCacheKeys(Deposit $deposit, $depositClosed = false): array
    {
        if (null == $deposit->user_id) {
            return [];
        }

        $keys = [];
        if (
            $depositClosed
            && cache()->get('userHasClosedDeposits.' . $deposit->user_id) !== true
        ) { // Clear deposit if first deposit has been closed
            $keys[] = 'userHasClosedDeposits.' . $deposit->user_id;
        }
        return $keys;
    }

    /**
     * @param Deposit $deposit
     * @return array
     */
    private function getCacheTags(Deposit $deposit): array
    {
        if (null == $deposit->user_id) {
            return [];
        }

        return [
            'userDeposits.' . $deposit->user_id,
            'lastCreatedDeposits',
            'depositsCount',
            'userDepositLimit.' . $deposit->user_id,
        ];
    }

    /**
     * Listen to the Deposit created event.
     *
     * @param Deposit $deposit
     * @return void
     * @throws
     */
    public function created(Deposit $deposit)
    {
        clearCacheByArray($this->getCacheKeys($deposit));
        clearCacheByTags($this->getCacheTags($deposit));
    }

    /**
     * Listen to the Deposit deleting event.
     *
     * @param Deposit $deposit
     * @return void
     * @throws
     */
    public function deleted(Deposit $deposit)
    {
        clearCacheByArray($this->getCacheKeys($deposit));
        clearCacheByTags($this->getCacheTags($deposit));
    }

    /**
     * Listen to the Deposit updating event.
     *
     * @param Deposit $deposit
     * @return void
     * @throws
     */
    public function updated(Deposit $deposit)
    {
        clearCacheByArray($this->getCacheKeys(
            $deposit,
            ($deposit->isDirty('active') && !$deposit->active)
        ));
        clearCacheByTags($this->getCacheTags($deposit));
    }
}