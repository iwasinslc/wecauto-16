@extends('layouts.customer')
@section('title', __('Website is disabled'))
@section('content')
    <article class="page">
        <div class="container">
            <div class="page-top">
                <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('Under Construction')}}</span>
                </div>
            </div>
            <section class="not-found"><img src="/assets/images/404.png" alt="">
                <h3 class="not-found__title">{{__('Page under')}} <span class="color-warning">{{__('construction')}}</span>
                </h3>
                <p class="not-found__subtitle">{{__('Will be available soon')}}
                </p>
{{--                <div class="not-found__buttons"><a class="btn btn--warning" href="#">Go to the main page</a>--}}
{{--                </div>--}}
            </section>
        </div>
    </article>
@endsection